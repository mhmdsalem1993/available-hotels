#!/usr/bin/env bash

./build.sh


java -jar config-server/target/config-server.jar &
CS_PID=$!

java -jar best-hotels/target/best-hotels.jar &
BH_PID=$!

java -jar crazy-hotels/target/crazy-hotels.jar &
CH_PID=$!

sleep 10

java -jar -Dspring.profiles.active=local target/available-hotels.jar &
AH_PID=$!

echo "CONFIG SERVER PID=$CS_PID ,CRAZY HOTELS PID = $CH_PID , BEST HOTELS PID $BH_PID , AVAILABLE HOTELS PID $AH_PID "

echo "$CS_PID $AH_PID $BH_PID $CH_PID" > .last_pid
