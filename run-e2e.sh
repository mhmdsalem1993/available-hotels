#!/usr/bin/env bash

./build.sh
docker-compose up -d
cd ./acceptance-test
mvn clean install