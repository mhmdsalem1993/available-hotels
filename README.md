# Available Hotels [![pipeline status](https://gitlab.com/mhmdsalem1993/available-hotels/badges/master/pipeline.svg)](https://gitlab.com/mhmdsalem1993/available-hotels/commits/master)
Service that retrieve list of Hotels based on search filters 

### Structure Of the Project : 

---

- available-hotels: is the main service which you query 
  for the list of available hotels
  For a sample request check [available-hotels.http](./available-hotels.http)
- best-hotels and crazy-hotels : are two simple services that are registered
  in available-hotels service to query them for the hotels
- config-server: is a centralized configuration server used by available-hotels to manage the configuration
- acceptance-test: is an End to End test for the solution using [cucumber](https://cucumber.io/).

Note : the available-hotels service is secured by Basic Auth ; The credintials are username as username and password as password , refer to [available-hotels.http](./available-hotels.http)

---

### Deployment:



**Notes** :
 
-  When you run local deployment **local profile is active** in **available-hotels** so the [application-local.yml](./config-server/src/main/resources/config/available-hotels/application-local.yml)
 
-  When you run containers deployment **docker profile is active** in **available-hotels** so the [application-docker.yml](./config-server/src/main/resources/config/available-hotels/application-docker.yml)


---  
```bash

# run in local machine

./deploy-local.sh

# this will store the running PIDs in .last_pid to stop local deployment 

./stop-local.sh

# run in docker compose

./deploy-containers.sh

# to package the jars 

./build.sh

# to run tests

./run-test.sh


```

### Extendability 

---

In case you want to add a new providers in the future ,
You only need to create your service and then map the request and response at 
in available-hotels configuration file which you can find it in [here](./config-server/src/main/resources/config/available-hotels) .

For now we only have two backing services which are mapped as following

````yml

available:
  hotels:
    providers:
      - name: BestHotel
        url: http://localhost:8055/api/v1/hotels
        mapping:
          request:
            fromDate: fromDate
            toDate: toDate
            city: city
            numberOfAdults: numberOfAdults
          response:
            hotelName: hotel
            fare: hotelFare
            amenities: roomAmenities
            rate: hotelRate

      - name: CrazyHotels
        url: http://localhost:8066/api/v1/hotels
        mapping:
          request:
            fromDate: from
            toDate: to
            city: city
            numberOfAdults: adultsCount
          response:
            hotelName: hotelName
            amenities: amenities
            rate: rate
            fare: price
            discount: discount

````

The keys in the [request](./src/main/java/com/example/availablehotels/services/contract/model/ListingRequest.java) 
and [response](./src/main/java/com/example/availablehotels/clients/contract/model/ProvidersResponse.java) are the one's inside available-hotels,
on the other hand the values belongs to the backing services.


---

We can extend the application more by add refresh scope ability [example](https://andressanchezblog.wordpress.com/2016/09/15/refresh-scope-in-spring-cloud/)



### Isolation 

[ProvidersClient](src/main/java/com/example/availablehotels/clients/contract/ProvidersClient.java) been created as interface to make it possible to change the implementation and to isolate the [HotelService](src/main/java/com/example/availablehotels/services/contract/HotelsService.java)
from the client type whether is http client or queue listener or what so ever.

### Testing

Unit test , Integration test for available-service  
Integration test for the two dummy services best-hotels,crazy-hotels
End to End test for the whole solution

```bash

# To run e2e test locally

./run-e2e.sh

```

### CI/CD

Its two stages one to run unit , integration tests and package artifacts to use them in the next stage which run the deployment inside docker-compose and run the e2e test against them 

