package com.example.acceptancetest;

import com.jayway.jsonpath.JsonPath;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Base64;
import java.util.Map;

public class HotelsStepDef extends SpringFeatureTest {

    private static final String TEMPLATE = "http://%s:%d/api/v1/hotels?fromDate=%s&toDate=%s&city=%s&numberOfAdults=%d";

    @Value("${available.hotels.host:localhost}")
    private String host;
    @Value("${available.hotels.port:8080}")
    private Integer port;

    @Autowired
    private RestTemplate restTemplate;

    private String response;

    @When("^Listing Hotels using credentials user=\"([^\"]*)\", password=\"([^\"]*)\" and body")
    public void listingHotels(String username, String password, Map<String, String> map) {
        String encoding = Base64.getEncoder().encodeToString(String.format("%s:%s", username, password).getBytes());
        RequestEntity<Void> request = RequestEntity
                .get(URI.create(createUrl(map)))
                .header("Authorization", "Basic " + encoding)
                .build();
        ResponseEntity<String> responseEntity = restTemplate.exchange(request, String.class);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        response = responseEntity.getBody();

    }

    private String createUrl(Map<String, String> map) {
        return String.format(TEMPLATE,
                host,
                port,
                map.get("fromDate"),
                map.get("toDate"),
                map.get("city"),
                Integer.valueOf(map.get("numberOfAdults")));
    }

    @Then("^Result should not be empty$")
    public void resultShouldNotBeEmpty() {
        Assert.assertNotNull(response);
    }

    @And("^\"([^\"]*)\" not null$")
    public void notNull(String path) {
        Assert.assertNotNull(JsonPath.parse(response).read(path));
    }
}
