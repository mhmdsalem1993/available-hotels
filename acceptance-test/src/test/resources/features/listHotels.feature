Feature: Listing Hotels

  Scenario: Listing Hotels from Available Hotels Service

    When Listing Hotels using credentials user="username", password="password" and body
      | fromDate       | 2019-02-15 |
      | toDate         | 2019-02-20 |
      | city           | AUH        |
      | numberOfAdults | 2          |

    Then Result should not be empty
    And "$.[0].fare" not null
    And "$.[0].amenities" not null
    And "$.[0].hotelName" not null
    And "$.[0].provider" not null