#!/usr/bin/env bash

kill -9 $(cat .last_pid)
rm .last_pid
