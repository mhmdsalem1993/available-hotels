#!/usr/bin/env bash

mvn clean install

cd best-hotels
mvn clean install

cd ../crazy-hotels
mvn clean install

cd ../config-server
mvn clean install