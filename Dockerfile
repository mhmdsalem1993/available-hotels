FROM openjdk:8u111-jdk-alpine
VOLUME /tmp
EXPOSE 8080
ADD target/available-hotels.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]