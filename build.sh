#!/usr/bin/env bash

mvn clean install -DskipTests

cd best-hotels
mvn clean install -DskipTests

cd ../crazy-hotels
mvn clean install -DskipTests

cd ../config-server
mvn clean install -DskipTests