package com.example.availablehotels.controllers;

import com.example.availablehotels.services.contract.HotelsService;
import com.example.availablehotels.services.contract.model.City;
import com.example.availablehotels.services.contract.model.HotelSummary;
import com.example.availablehotels.services.contract.model.ListingRequest;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class HotelsControllerTest {

    @Mock
    private HotelsService hotelsService;
    @InjectMocks
    private HotelsController controller;

    @Test
    public void listWithValidFields() {
        ListingRequest request = TestHelper.newRequest();
        List<HotelSummary> summaries = Collections.singletonList(TestHelper.newHotelSummary());
        Mockito.when(hotelsService.list(request)).thenReturn(summaries);
        ResponseEntity response = controller.list(request);
        Mockito.verify(hotelsService).list(request);
        Assertions.assertThat(response.getBody()).isEqualTo(summaries);
    }

    @Test
    public void list() {
        ListingRequest request = TestHelper.newRequest();
        List<HotelSummary> summaries = Collections.singletonList(TestHelper.newHotelSummary());
        Mockito.when(hotelsService.list(request)).thenReturn(summaries);
        ResponseEntity response = controller.list(request);
        Mockito.verify(hotelsService).list(request);
        Assertions.assertThat(response.getBody()).isEqualTo(summaries);
    }

}