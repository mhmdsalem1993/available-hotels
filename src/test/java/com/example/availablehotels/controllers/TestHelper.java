package com.example.availablehotels.controllers;

import com.example.availablehotels.configuration.Provider;
import com.example.availablehotels.services.contract.model.City;
import com.example.availablehotels.services.contract.model.HotelSummary;
import com.example.availablehotels.services.contract.model.ListingRequest;

import java.time.LocalDate;
import java.util.Arrays;

import static com.example.availablehotels.configuration.Provider.*;
import static com.example.availablehotels.configuration.Provider.Mapping.*;

public final class TestHelper {

    private TestHelper() {
    }

    public static HotelSummary newHotelSummary() {
        HotelSummary hotelSummary = new HotelSummary();
        hotelSummary.setProvider("CrazyHotels");
        hotelSummary.setAmenities(Arrays.asList("Swimming Pool", "Spa", "Room Service", "Valet Parking"));
        hotelSummary.setHotelName("Pegasus Wings");
        return hotelSummary;
    }

    public static ListingRequest newRequest() {
        ListingRequest request = new ListingRequest();
        request.setFromDate(LocalDate.now());
        request.setToDate(LocalDate.now().plusDays(4));
        request.setCity(City.AUH);
        request.setNumberOfAdults(4);
        return request;
    }

    public static Provider createProvider() {
        Provider provider = new Provider();
        provider.setName("LuxuryHotels");
        provider.setUrl("http://localhost:5555/api/v1/hotels");
        Mapping mapping = new Mapping();
        RequestMapping request = new RequestMapping();
        request.setCity("city");
        request.setFromDate("from");
        request.setToDate("to");
        request.setNumberOfAdults("countOfAdults");
        mapping.setRequest(request);
        ResponseMapping response = new ResponseMapping();
        response.setAmenities("amenities");
        response.setHotelName("name");
        response.setFare("price");
        response.setRate("rate");
        mapping.setResponse(response);
        provider.setMapping(mapping);
        return provider;
    }
}
