package com.example.availablehotels.controllers;

import com.example.availablehotels.services.contract.HotelsService;
import com.example.availablehotels.services.contract.model.City;
import com.example.availablehotels.services.contract.model.HotelSummary;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.Collections;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(HotelsController.class)
@WithMockUser
public class HotelsControllerIT {

    @MockBean
    private HotelsService hotelsService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void list() throws Exception {

        HotelSummary summary = TestHelper.newHotelSummary();
        Mockito.when(hotelsService.list(Mockito.any()))
                .thenReturn(Collections.singletonList(summary));

        String urlTemplate = "/api/v1/hotels?fromDate=%s&toDate=%s&city=%s&numberOfAdults=%d";
        String url = String.format(urlTemplate, LocalDate.now(), LocalDate.now().plusDays(2), City.AUH, 4);

        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.size()", is(1)))
                .andExpect(jsonPath("$.[0].provider", is(summary.getProvider())))
                .andExpect(jsonPath("$.[0].hotelName", is(summary.getHotelName())))
                .andExpect(jsonPath("$.[0].fare", is(summary.getFare())))
                .andExpect(jsonPath("$.[0].amenities", is(summary.getAmenities())));

    }

    @Test
    public void listWithInvalidParams() throws Exception {
        mockMvc.perform(get("/api/v1/hotels"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.size()", is(4)))
                .andExpect(jsonPath("$..field", hasItem("fromDate")))
                .andExpect(jsonPath("$..message", hasItem("fromDate is required")))
                .andExpect(jsonPath("$..field", hasItem("toDate")))
                .andExpect(jsonPath("$..message", hasItem("toDate is required")))
                .andExpect(jsonPath("$..field", hasItem("city")))
                .andExpect(jsonPath("$..message", hasItem("city is required")))
                .andExpect(jsonPath("$..field", hasItem("numberOfAdults")))
                .andExpect(jsonPath("$..message", hasItem("numberOfAdults is required")));

    }
}