package com.example.availablehotels.controllers;

import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.BAD_REQUEST;


public class ExceptionsHandlerTest {

    private static final String FROM_DATE_KEY = "fromDate";
    private static final String FROM_DATE_MESSAGE = "fromDate is required";

    @Test
    public void handle() {
        ExceptionsHandler handler = new ExceptionsHandler();
        BindException bindException = mock(BindException.class);
        FieldError fieldError = new FieldError(FROM_DATE_KEY, FROM_DATE_KEY, FROM_DATE_MESSAGE);

        when(bindException.getFieldErrors()).thenReturn(Collections.singletonList(fieldError));

        ResponseEntity<List<Violation>> responseEntity = handler.handle(bindException);

        assertThat(responseEntity.getStatusCode()).isEqualTo(BAD_REQUEST);
        assertThat(responseEntity.getBody()).contains(new Violation(FROM_DATE_KEY, FROM_DATE_MESSAGE));
    }
}