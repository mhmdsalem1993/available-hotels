package com.example.availablehotels.services;

import com.example.availablehotels.clients.contract.ProvidersClient;
import com.example.availablehotels.controllers.TestHelper;
import com.example.availablehotels.services.contract.model.HotelSummary;
import com.example.availablehotels.services.contract.model.ListingRequest;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class HotelsServiceImplTest {

    @Mock
    private ProvidersClient client;

    @InjectMocks
    private HotelsServiceImpl service;


    @Test
    public void list() {
        ListingRequest request = TestHelper.newRequest();
        List<HotelSummary> hotelSummaries = Collections.singletonList(TestHelper.newHotelSummary());

        when(client.getHotels(any())).thenReturn(hotelSummaries);

        List<HotelSummary> resultSummaries = service.list(request);

        verify(client).getHotels(request);
        Assertions.assertThat(resultSummaries).isEqualTo(hotelSummaries);
    }
}