package com.example.availablehotels.clients;

import com.example.availablehotels.configuration.Provider;
import com.example.availablehotels.controllers.TestHelper;
import com.example.availablehotels.services.contract.model.HotelSummary;
import com.example.availablehotels.services.contract.model.ListingRequest;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProvidersClientImplTest {

    private static final double FARE_VALUE = 20.00;
    private static final String HOTEL_NAME_VALUE = "Sparta";
    private static final List<String> AMENITIES_VALUE = singletonList("Room Service");

    @Mock
    private RestTemplate restTemplate;

    private Provider provider;

    private ProvidersClientImpl providersClient;

    @Before
    public void setUp() {
        provider = TestHelper.createProvider();
        providersClient = new ProvidersClientImpl(singletonList(provider), restTemplate);
    }


    @Test
    public void whenInvokingShouldReturnListOfHotels() {

        ListingRequest request = TestHelper.newRequest();

        List<Map<String, Object>> response = createProviderResponse();

        when(restTemplate.getForObject(anyString(), any())).thenReturn(response);

        List<HotelSummary> hotels = providersClient.getHotels(request);

        Assertions.assertThat(hotels.get(0).getFare().doubleValue()).isEqualTo(FARE_VALUE);
        Assertions.assertThat(hotels.get(0).getAmenities()).isEqualTo(AMENITIES_VALUE);
        Assertions.assertThat(hotels.get(0).getHotelName()).isEqualTo(HOTEL_NAME_VALUE);
        Assertions.assertThat(hotels.get(0).getProvider()).isEqualTo(provider.getName());

    }

    @Test(expected = ProvidersClientException.class)
    public void whenInvokingAndErrorOccurShouldThrowBusinessException() {

        ListingRequest request = TestHelper.newRequest();

        when(restTemplate.getForObject(anyString(), any())).thenThrow(RestClientException.class);

        providersClient.getHotels(request);

    }

    private List<Map<String, Object>> createProviderResponse() {
        Map<String, Object> map = new HashMap<>();
        map.put(provider.getMapping().getResponse().getRate(), "****");
        map.put(provider.getMapping().getResponse().getFare(), FARE_VALUE);
        map.put(provider.getMapping().getResponse().getAmenities(), AMENITIES_VALUE);
        map.put(provider.getMapping().getResponse().getHotelName(), HOTEL_NAME_VALUE);
        return singletonList(map);
    }


}