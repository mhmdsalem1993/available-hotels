package com.example.availablehotels.configuration;

import lombok.Data;

@Data
public class Provider {

    private String name;
    private String url;
    private Mapping mapping;

    @Data
    public static class Mapping {
        private RequestMapping request;
        private ResponseMapping response;

        @Data
        public static class RequestMapping {
            private String fromDate;
            private String toDate;
            private String city;
            private String numberOfAdults;
        }

        @Data
        public static class ResponseMapping {
            private String hotelName;
            private String fare;
            private String amenities;
            private String rate;
            private String price;
            private String discount;
        }
    }

}
