package com.example.availablehotels.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@ConfigurationProperties("available.hotels")
@Data
public class AvailableHotelsConfiguration {

    private List<Provider> providers;

}
