package com.example.availablehotels.configuration;

import com.example.availablehotels.clients.ProvidersClientImpl;
import com.example.availablehotels.clients.contract.ProvidersClient;
import com.example.availablehotels.services.HotelsServiceImpl;
import com.example.availablehotels.services.contract.HotelsService;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableConfigurationProperties(AvailableHotelsConfiguration.class)
public class ApplicationConfiguration {


    @Bean
    public HotelsService hotelsService(ProvidersClient client) {
        return new HotelsServiceImpl(client);
    }

    @Bean
    public ProvidersClient providersClient(AvailableHotelsConfiguration configuration, RestTemplate restTemplate) {
        return new ProvidersClientImpl(configuration.getProviders(), restTemplate);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

}
