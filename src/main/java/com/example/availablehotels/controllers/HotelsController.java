package com.example.availablehotels.controllers;

import com.example.availablehotels.services.contract.HotelsService;
import com.example.availablehotels.services.contract.model.ListingRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/hotels")
public class HotelsController {

    private final HotelsService hotelsService;

    public HotelsController(HotelsService hotelsService) {
        this.hotelsService = hotelsService;
    }

    @GetMapping
    public ResponseEntity list(@Valid ListingRequest request) {
        return ResponseEntity.ok(hotelsService.list(request));
    }
}
