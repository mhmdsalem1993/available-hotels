package com.example.availablehotels.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionsHandler {


    @ExceptionHandler
    public ResponseEntity<List<Violation>> handle(BindException e) {
        List<FieldError> fieldErrors = e.getFieldErrors();
        List<Violation> violations = fieldErrors.stream()
                .map(fieldError -> new Violation(fieldError.getField(), fieldError.getDefaultMessage()))
                .collect(Collectors.toList());
        return ResponseEntity.badRequest().body(violations);
    }
}
