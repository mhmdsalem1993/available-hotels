package com.example.availablehotels.clients.contract;

import com.example.availablehotels.services.contract.model.HotelSummary;
import com.example.availablehotels.services.contract.model.ListingRequest;

import java.util.List;

public interface ProvidersClient {
    List<HotelSummary> getHotels(ListingRequest request);
}
