package com.example.availablehotels.clients.contract.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class ProvidersResponse {
    private String providerName;
    private String hotelName;
    private Double fare;
    private List<String> amenities;
    private String rate;
    private Double price;
    private String discount;
}
