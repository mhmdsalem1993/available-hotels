package com.example.availablehotels.clients;

import com.example.availablehotels.clients.contract.ProvidersClient;
import com.example.availablehotels.clients.contract.model.ProvidersResponse;
import com.example.availablehotels.configuration.Provider;
import com.example.availablehotels.services.contract.model.HotelSummary;
import com.example.availablehotels.services.contract.model.ListingRequest;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.example.availablehotels.configuration.Provider.Mapping;
import static com.example.availablehotels.configuration.Provider.Mapping.RequestMapping;
import static com.example.availablehotels.configuration.Provider.Mapping.ResponseMapping;

public class ProvidersClientImpl implements ProvidersClient {

    private final List<Provider> providers;
    private final RestTemplate restTemplate;

    public ProvidersClientImpl(List<Provider> providers, RestTemplate restTemplate) {
        this.providers = providers;
        this.restTemplate = restTemplate;
    }


    @Override
    public List<HotelSummary> getHotels(ListingRequest request) {
        try {
            return providers
                    .stream()
                    .map(provider ->
                            {
                                Mapping mapping = provider.getMapping();
                                String url = buildRequestUrl(request, mapping.getRequest(), provider.getUrl());
                                List<Map<String, Object>> response = restTemplate.getForObject(url, List.class);
                                return getProviderResponse(provider, mapping.getResponse(), response);
                            }
                    )
                    .flatMap(List::stream)
                    .sorted((o1, o2) -> o2.getRate().compareTo(o1.getRate()))
                    .map(this::toHotelSummary)
                    .collect(Collectors.toList());
        } catch (Exception e){
            throw  new ProvidersClientException(e);
        }
    }

    private List<ProvidersResponse> getProviderResponse(Provider provider,
                                                        ResponseMapping responseMapping,
                                                        List<Map<String, Object>> response) {
        return Objects.requireNonNull(response)
                .stream()
                .map(items -> mapResponseToProviderResponse(provider.getName(), responseMapping, items))
                .collect(Collectors.toList());
    }

    private String buildRequestUrl(ListingRequest request,
                                   RequestMapping requestMapping,
                                   String baseUrl) {
        String urlTemplate = baseUrl + "?"
                + requestMapping.getFromDate() + "=%s&"
                + requestMapping.getToDate() + "=%s&"
                + requestMapping.getCity() + "=%s&"
                + requestMapping.getNumberOfAdults() + "=%s";

        return createRequestUrl(urlTemplate, request);
    }

    private ProvidersResponse mapResponseToProviderResponse(String providerName,
                                                            ResponseMapping responseMapping,
                                                            Map<String, Object> items) {
        String hotelName = (String) items.get(responseMapping.getHotelName());
        List<String> amenities = (List<String>) items.get(responseMapping.getAmenities());
        String discount = (String) items.get(responseMapping.getDiscount());
        Double fare = ((Double) items.get(responseMapping.getFare()));
        Double price = ((Double) items.get(responseMapping.getPrice()));
        String rate = (String) items.get(responseMapping.getRate());
        return new ProvidersResponse(providerName, hotelName, fare, amenities, rate, price, discount);
    }

    private HotelSummary toHotelSummary(ProvidersResponse response) {
        HotelSummary hotelSummary = new HotelSummary();
        hotelSummary.setHotelName(response.getHotelName());
        hotelSummary.setProvider(response.getProviderName());
        hotelSummary.setAmenities(response.getAmenities());
        hotelSummary.setFare(BigDecimal.valueOf(response.getFare()));
        return hotelSummary;
    }

    private String createRequestUrl(String urlTemplate, ListingRequest request) {
        return String.format(
                urlTemplate,
                request.getFromDate(),
                request.getToDate(),
                request.getCity(),
                request.getNumberOfAdults());
    }
}
