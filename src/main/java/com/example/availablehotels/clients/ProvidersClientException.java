package com.example.availablehotels.clients;

public class ProvidersClientException extends RuntimeException {
    public ProvidersClientException(Exception e) {
        super(e);
    }
}
