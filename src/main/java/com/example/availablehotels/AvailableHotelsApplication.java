package com.example.availablehotels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvailableHotelsApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvailableHotelsApplication.class, args);
    }

}

