package com.example.availablehotels.services.contract.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@Data
public class ListingRequest {

    @NotNull(message = "fromDate is required")
    private LocalDate fromDate;

    @NotNull(message = "toDate is required")
    private LocalDate toDate;

    @NotNull(message = "city is required")
    private City city;

    @Positive
    @NotNull(message = "numberOfAdults is required")
    private Integer numberOfAdults;
}
