package com.example.availablehotels.services.contract;

import com.example.availablehotels.services.contract.model.ListingRequest;
import com.example.availablehotels.services.contract.model.HotelSummary;

import java.util.List;

public interface HotelsService {

    List<HotelSummary> list(ListingRequest request);

}
