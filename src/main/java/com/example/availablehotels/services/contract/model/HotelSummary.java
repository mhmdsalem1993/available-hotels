package com.example.availablehotels.services.contract.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class HotelSummary {
    private String provider;
    private String hotelName;
    private BigDecimal fare;
    private List<String> amenities;
}
