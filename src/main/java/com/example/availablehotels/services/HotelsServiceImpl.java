package com.example.availablehotels.services;

import com.example.availablehotels.clients.contract.ProvidersClient;
import com.example.availablehotels.services.contract.HotelsService;
import com.example.availablehotels.services.contract.model.HotelSummary;
import com.example.availablehotels.services.contract.model.ListingRequest;

import java.util.List;

public class HotelsServiceImpl implements HotelsService {

    private final ProvidersClient client;

    public HotelsServiceImpl(ProvidersClient client) {
        this.client = client;
    }

    @Override
    public List<HotelSummary> list(ListingRequest request) {
        return client.getHotels(request);
    }

}
