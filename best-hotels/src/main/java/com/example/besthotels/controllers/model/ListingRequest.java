package com.example.besthotels.controllers.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ListingRequest {


    private LocalDate fromDate;

    private LocalDate toDate;

    private String city;

    private Integer numberOfAdults;

}
