package com.example.besthotels.controllers;

import com.example.besthotels.controllers.model.ListingRequest;
import com.example.besthotels.services.contract.HotelsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hotels")
public class HotelsController {
    private final HotelsService hotelsService;

    public HotelsController(HotelsService hotelsService) {
        this.hotelsService = hotelsService;
    }

    @GetMapping
    public ResponseEntity list(ListingRequest request) {
        return ResponseEntity.ok(hotelsService.list(request));
    }
}
