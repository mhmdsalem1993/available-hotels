package com.example.besthotels;

import com.example.besthotels.services.HotelsServiceImpl;
import com.example.besthotels.services.contract.HotelsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public HotelsService hotelsService() {
        return new HotelsServiceImpl();
    }
}
