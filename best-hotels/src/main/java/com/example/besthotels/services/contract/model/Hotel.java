package com.example.besthotels.services.contract.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
public class Hotel {

    private String hotel;
    private Double hotelFare;
    private List<String> roomAmenities;
    private String hotelRate;

}
