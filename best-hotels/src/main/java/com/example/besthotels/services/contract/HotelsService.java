package com.example.besthotels.services.contract;

import com.example.besthotels.controllers.model.ListingRequest;
import com.example.besthotels.services.contract.model.Hotel;

import java.util.List;

public interface HotelsService {
    List<Hotel> list(ListingRequest request);
}
