package com.example.besthotels.services;

import com.example.besthotels.controllers.model.ListingRequest;
import com.example.besthotels.services.contract.HotelsService;
import com.example.besthotels.services.contract.model.Hotel;

import java.util.List;

import static java.util.Arrays.asList;

public class HotelsServiceImpl implements HotelsService {
    @Override
    public List<Hotel> list(ListingRequest request) {
        return asList(
                new Hotel("Pegasus", 450.00, asList("Swimming pools", "Internet access"), "****"),
                new Hotel("Midas", 100.00, asList("Television", "Kitchen facilities"), "**"),
                new Hotel("Zeus", 20.00, asList("Hair dryer", "Internet access"), "*"),
                new Hotel("Hera", 510.00, asList("Towels", "Room Service"), "*****"),
                new Hotel("Achilles", 205.00, asList("Television", "Room Service", "Internet access"), "***"),
                new Hotel("Medusa", 500.00, asList("Towels", "Kitchen facilities"), "*****"),
                new Hotel("Odysseus", 50.00, asList("Swimming pools", "Room Service"), "**"),
                new Hotel("Hector", 360.00, asList("Hair dryer", "Television"), "****")
        );
    }
}
