package com.example.crazyhotels.controllers.model;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ListingRequest {


    private LocalDate from;

    private LocalDate to;

    private String city;

    private Integer adultsCount;

}
