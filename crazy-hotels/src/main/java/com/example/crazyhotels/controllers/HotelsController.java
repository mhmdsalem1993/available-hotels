package com.example.crazyhotels.controllers;

import com.example.crazyhotels.controllers.model.ListingRequest;
import com.example.crazyhotels.services.contract.HotelsService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hotels")
public class HotelsController {
    private final HotelsService hotelsService;

    public HotelsController(HotelsService hotelsService) {
        this.hotelsService = hotelsService;
    }

    @GetMapping
    public ResponseEntity list(ListingRequest request) {
        return ResponseEntity.ok(hotelsService.list(request));
    }
}
