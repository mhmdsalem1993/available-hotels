package com.example.crazyhotels;

import com.example.crazyhotels.services.HotelsServiceImpl;
import com.example.crazyhotels.services.contract.HotelsService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public HotelsService hotelsService() {
        return new HotelsServiceImpl();
    }
}
