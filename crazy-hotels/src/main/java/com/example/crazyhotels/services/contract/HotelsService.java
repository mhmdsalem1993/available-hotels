package com.example.crazyhotels.services.contract;

import com.example.crazyhotels.controllers.model.ListingRequest;
import com.example.crazyhotels.services.contract.model.Hotel;

import java.util.List;

public interface HotelsService {
    List<Hotel> list(ListingRequest request);
}
