package com.example.crazyhotels.services;

import com.example.crazyhotels.controllers.model.ListingRequest;
import com.example.crazyhotels.services.contract.HotelsService;
import com.example.crazyhotels.services.contract.model.Hotel;

import java.util.List;

import static java.util.Arrays.asList;

public class HotelsServiceImpl implements HotelsService {
    @Override
    public List<Hotel> list(ListingRequest request) {
        return asList(
                new Hotel("Maia", 450.00, "10%", asList("Swimming pools", "Internet access"), "****"),
                new Hotel("Saturn", 100.00, "50%", asList("Television", "Kitchen facilities"), "**"),
                new Hotel("Honos", 20.00, "20%", asList("Hair dryer", "Internet access"), "*"),
                new Hotel("Caca", 510.00, null, asList("Towels", "Room Service"), "*****"),
                new Hotel("Moneta", 205.00, "10%", asList("Television", "Room Service", "Internet access"), "***"),
                new Hotel("Nascio", 500.00, "10%", asList("Towels", "Kitchen facilities"), "*****"),
                new Hotel("Acca", 50.00, "10%", asList("Swimming pools", "Room Service"), "**"),
                new Hotel("Dryad", 360.00, null, asList("Hair dryer", "Television"), "****")
        );
    }
}
