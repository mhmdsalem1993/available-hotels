package com.example.crazyhotels.services.contract.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Hotel {

    private String hotelName;
    private Double price;
    private String discount;
    private List<String> amenities;
    private String rate;

}
