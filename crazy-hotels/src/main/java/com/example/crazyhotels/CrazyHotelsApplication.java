package com.example.crazyhotels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrazyHotelsApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrazyHotelsApplication.class, args);
    }

}

